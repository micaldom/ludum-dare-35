require(... .. "/lib")

-- Load resources
cb = require("bgm")
cf = require("fnt")
cg = require("gfx")
ci = require(... .. "/input")
cm = require(... .. "/maps")
cs = require(... .. "/states")
cx = require("sfx")

-- Load pylons code
require(... .. "/pylons")

-- Load players code
require(... .. "/player")
