-- Chikun :: 2016
-- Code for player


-- Player object with images
player = { }

-- Function to create the player
-- Input : Nil
-- Output: Nil
function playerCreate()

	-- Place player at the desired x and y positions
	player.x = 32
	player.y = GAME_HEIGHT / 2

	-- Give player desired width and height
	player.w = 32
	player.h = 32

	-- Initialise the players y movement speed
	player.y_move = 0
end

-- Function to update player
-- Input : Delta Time
-- Output: Nil
function playerUpdate(dt)

	-- Check if player is dead
	if not(player_dead) then

		-- If in air
		if (mode == 1) then

			-- Gravity goes down
			gravity = 1

		-- If in water
		elseif (mode == 2) then

			-- Gravity goes up
			gravity = - 1
		end

		-- Check if mouse has just been clicked but not on ground
		if ((lm.isDown(1) and mouse_is_down == false) and not(mode == 3)) then

			-- Set players y movement speed to a set amount
			player.y_move =  (- 5 - (points / 30)) * gravity

			-- Set mouse to being held
			mouse_is_down = true
		else

			-- If mouse has just been clicked while on the ground
			if (lm.isDown(1) and mouse_is_down == false) then

				-- Invert gravity
				gravity = - gravity

				-- Set mouse to being held
				mouse_is_down = true
			end

			-- If on ground
			if (mode == 3) then

				-- Set player speed to a constant
				player.y_move = (15 + points / 10) * gravity / 6
			else

				-- Apply gravity to players y movement speed
				player.y_move = player.y_move + dt *
								(15 + points / 10) * gravity
			end

			-- Check is mouse is released
			if not(lm.isDown(1)) then

				-- Set mouse being held down to false
				mouse_is_down = false
			end
		end

		-- Move player based on y movement speed
		player.y = player.y + player.y_move

		-- Make sure there is a floor to collide with
		if floor_hole.x then

			-- If player is colliding with the floors hole
			if (player.x + player.w >= floor_hole.x) then

				if (
						((mode == 2) and
							(player.y <= floor_hole.y + floor_hole.h)) or
						((mode == 1) and
							(player.y + player.h >= floor_hole.y)) or
						((mode == 3) and (((floor_hole.mode == 1) and
									(player.y <= floor_hole.y + floor_hole.h))
								or ((floor_hole.mode == 2) and
									(player.y + player.h >= floor_hole.y)))))
					then

					if (mode == 3) then

						player.y_move = player.y_move * 3
					end

					-- Change game mode
					mode = floor_hole.mode

					-- Find out what new game mode is
					if (floor_hole.mode == 2) then

						-- Move player accordingly
						player.y = 64
					elseif (floor_hole.mode == 3) then

						-- Move player accordingly
						player.y = GAME_HEIGHT / 2 - player.h / 2
					else

						-- Move player accordingly
						player.y = GAME_HEIGHT - 96
					end

					-- Clear field of pylons and floor holes
					floor_hole = { }
					pylons = { }
					pylon_count = 0
					pylon_spawn_timer = 0

					-- Reset y movement speed if ground
					if (mode == 3) then

						player.y_move = 0
					end
				end
			end
		end
	end

	-- If player hits spikes on top or bottom
	if ((player.y <= 32) or (player.y + player.h >= GAME_HEIGHT - 32)) then

		-- Kill player
		playerKill(dt)
	end
end

-- Function to draw player
-- Input : Nil
-- Output: Nil
function playerDraw()

	-- set color to black
	lg.setColor(255, 255, 255)

	if (mode == 2) then
		image = gfx.penguin
	elseif (mode == 1) then
		image = gfx.bird
	else
		image = gfx.emu
	end

	-- Draw player
	lg.draw(image, player.x, player.y)
end

-- Function to kill the player
-- Input : Delta Time
-- Output: Nil
function playerKill(dt)

	cs:load('death')
end
