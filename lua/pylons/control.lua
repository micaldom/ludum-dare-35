-- Chikun :: 2016
-- Code to control the pylons

pylons = { }

floor_hole = { }

-- Functions to create new pylons
-- Input : Nil
-- Output: Nil
function pylonCreate()

	-- Increase count of number of pylons
	pylon_count = pylon_count + 1

	-- Create temporary pylon
	local new_pylon = { }

	if (pylon_count == 5) then

		local y_pos = 0

		local new_mode = math.random(1, 3)

		while (new_mode == mode) do

			new_mode = math.random(1, 3)
		end

		if (new_mode == 1) then

			y_pos = 0

		elseif (new_mode == 2) then

			y_pos = GAME_HEIGHT - 64

		else

			if (mode == 1) then

				y_pos = GAME_HEIGHT - 64
			else

				y_pos = 0
			end
		end

		floor_hole = {
			x = GAME_WIDTH + 64,
			y = y_pos,
			w = GAME_WIDTH * 2,
			h = 64,
			mode = new_mode
		}
	end

	if (pylon_count > 5) then

		-- Reset pylon count
		pylon_count = pylon_count - 5

		-- Tell what the temporary pylon is
		new_pylon = {
			x = GAME_WIDTH,
			y = GAME_HEIGHT / 2,
			w = 64,
			h = 0
		}
	else

		-- Create a position for top of pylon hole
		local y_pos = math.random(32, GAME_HEIGHT - 32 -
									math.max((240 - points), 64))

		-- Tell what the temporary pylon is
		new_pylon = {
			x = GAME_WIDTH,
			y = y_pos,
			w = 64,
			h = math.max((240 - points), 64),
			scored_off = false
		}
	end

	-- Add temporary pylon to the table of all the pylons
	table.insert(pylons, new_pylon)
end

-- Function to update the pylons
-- Input : Delta Time
-- Output: Nil
function pylonUpdate(dt)

	if not(player_dead) then
		-- Cycle through table of pylons
		for key, pylon in ipairs(pylons) do

			-- Move pylons
			pylon.x = pylon.x - dt * (50 + points)
		end

		if floor_hole.x then

			floor_hole.x = floor_hole.x - dt * (50 + points)
		end

		-- Prevent running code if no pylons
		if (#pylons > 0) then

			-- If first pylon is off the screen
			if (pylons[1].x + pylons[1].w < 0) then

				-- Remove pylon from the game
				table.remove(pylons, 1)
			end
		end
	end

	-- Preven running if no pylons
	if (#pylons > 0) then

		-- If pylon is in line with player
		if ((pylons[1]. x < player.x + player.w) and
				(pylons[1].x + pylons[1].w > player.x)) then

			-- Check if player is within gap of the pylon
			if ((player.y < pylons[1].y) or
					(player.y + player.h > pylons[1].y + pylons[1].h)) then

				playerKill(dt)

			-- Check if player hasn't scored off the current pylon
			elseif (pylons[1].scored_off == false) then

				-- Add points
				points = points + 1

				-- Set pylon to have scored off of
				pylons[1].scored_off = true
			end
		end
	end
end

-- Function to draw the pylons
-- Input : Nil
-- Output: Nil
function pylonDraw()

	-- Cycle through table of pylons
	for key, pylon in ipairs(pylons) do

		-- Temporary x and y to draw with
		local x = math.floor(pylon.x)
		local y = math.floor(pylon.y)

		-- Set draw color to white
		lg.setColor(255, 255, 255)
		-- Draw upper pipe
		lg.draw(gfx.pipe, x, y - GAME_HEIGHT)
		-- Draw lower pipe
		lg.draw(gfx.pipe, x, y + pylon.h + GAME_HEIGHT, math.pi, 1, 1, 64, 0)
	end

	if floor_hole.x then

		local image = gfx.air
		lg.setColor(255, 255, 255)

		if (floor_hole.mode == 2) then

			image = gfx.water
		elseif (floor_hole.mode == 3) then

			image = gfx.grass
		end

		lg.draw(image, floor_hole.x, floor_hole.y)
	end
end
