-- chikun :: 2014-2015
-- Death state


-- Temporary state, removed at end of script
local DeathState = class(PrototypeClass, function(new_class) end)

-- Place to place highscore
local high_score = - 1

-- Boolean to tell if just breached high score
local record = false

-- Prevent immediately leaving death state
local delay = 0

-- On state create
function DeathState:create()

	record = false

	delay = 1

	if (high_score < points) then

		record = true

		high_score = points
	end
end


-- On state update
function DeathState:update(dt)

	if (delay > 0) then

		delay = delay - dt
	end

	if (lm.isDown(1) and delay <= 0) then
		cs:change('play')
	end
end


-- On state draw
function DeathState:draw()

	cs.states.play:draw()

	lg.setColor(0, 0, 0, 192)

	lg.rectangle('fill', 0, 0, GAME_WIDTH, GAME_HEIGHT)

	local text = ""

	if (record) then

		text = "Congratulations! Your new high score is " .. points .. "."
	else

		text = "Bad luck. \n Your highscore is " .. high_score .. ".\nYour score was " .. points .. ". Better luck next time."
	end

	lg.setColor(255, 255, 255)
	lg.printf(text .. "\nClick to start again.",
			  GAME_WIDTH / 8, GAME_HEIGHT / 2 - 128, GAME_WIDTH * 3 / 4, 'center')
end


-- On state kill
function DeathState:kill()

	cs.states.play:kill()
end


-- Transfer data to state loading script
return DeathState
