-- chikun :: 2014-2015
-- Play state

-- Temporary state, removed at end of script
local PlayState = class(PrototypeClass, function(new_class) end)

-- Timer for spawning new pylons
local pylon_spawn_timer = 0

-- Timer for moving spikes
local spike_offset = 0

-- Timer for moving the background
local background_offset = 0

-- Check if mouse is being held down
local mouse_is_down = false

-- On state create
function PlayState:create()

	-- Create gravity
	gravity = 1

	-- amount of points player has accumulated
	points = 0

	-- How many pylons have been spawned
	pylon_count = 0

	-- Set mode to air mode
	mode = 1

	-- Set pylon spawning time to 0
	pylon_spawn_timer = 0

	-- State that player is not dead
	player_dead = false

	-- Death timer set to 0
	death_timer = 0

	-- Create player
	playerCreate()
end


-- On state update
function PlayState:update(dt)


	-- Update player
	playerUpdate(dt)

	-- Prevent running code if player is dead
	if not(player_dead) then

		-- Change the spike off set
		spike_offset = spike_offset + (dt * (50 + points))

		-- If spike offset is greater than width of spike
		if spike_offset >= 8 then

			-- Move spikes back
			spike_offset = spike_offset - 8
		end

		-- Move background
		background_offset = background_offset + (dt * (50 + points))

		if background_offset >= GAME_WIDTH then

			background_offset = background_offset - GAME_WIDTH
		end

		-- Check if new pylon needs to be spawned
		if pylon_spawn_timer <= 0 then

			-- Set new spawn timer to 10 - points stuff to min of 1 seconds
			pylon_spawn_timer = math.max(5 - (points / 30), 0.5)

			-- Create a new Pylon
			pylonCreate()
		else

			-- count down pylon spawn timer
			pylon_spawn_timer = pylon_spawn_timer - dt
		end
	end

	-- Normal update for pylon
	pylonUpdate(dt)
end


-- On state draw
function PlayState:draw()

	-- Set color to white
	lg.setColor(255, 255, 255)

	-- Determine image for background
	local bg_image = gfx.sky

	if (mode == 2) then

		bg_image = gfx.sea
	elseif (mode == 3) then

		bg_image = gfx.ground
	end

	-- Draw background
	lg.draw(bg_image, 0 - background_offset, 0)
	lg.draw(bg_image, GAME_WIDTH - background_offset, 0)

	-- Set color to White
	lg.setColor(255, 255, 255)

	-- Local x position
	local x_pos = 0

	-- While our current drawing position is less than the games width
	while x_pos < GAME_WIDTH + 8 do

		-- Draw spike on floor
		lg.draw(gfx.spike, x_pos - math.floor(spike_offset), GAME_HEIGHT - 32)
		-- Draw spike on roof
		lg.draw(gfx.spike, x_pos + 8 - math.floor(spike_offset), 32, math.pi)

		-- Increase where we are drawing in the x_position
		x_pos = x_pos + 8
	end

	-- Draw pylons
	pylonDraw()

	-- Draw player
	playerDraw()

	-- Set color to red
	lg.setColor(255, 0, 0)

	-- Set font to normal font
	lg.setFont(cf.normal)

	-- Write the score
	lg.print(points, GAME_WIDTH - 64, 32)

	-- reset drawing off set to origin
	lg.origin()
end


-- On state kill
function PlayState:kill()

	pylons = { }
	player = { }
	floor_hole = { }
end


-- Transfer data to state loading script
return PlayState
